
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let token = localStorage.getItem("token")
let labelResult = document.querySelector('#labelResult')

fetch(`${baseUrl()}/api/courses/activate/${courseId}`, {
	method: "PUT",
	headers: {
		"Authorization": `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if(data){
		alert("Course Activated.")
		labelResult.innerHTML = 'Course activated.'
	} else {
		alert("Something Went Wrong.")
	}
})