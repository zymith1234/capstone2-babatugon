let adminUser = localStorage.getItem('isAdmin') 
let addButton = document.querySelector('#adminButton')
let cardFooter 
let container = document.querySelector("#coursesContainer")
let token = localStorage.getItem("token")
let courseData
let msgForGuest = document.querySelector("#forGuest")
let msg
let bookNow = document.querySelector("#bookNow")

let enrollments = []
fetch(`${baseUrl()}/api/users/details`,{
	method: "GET",
	headers: {
		"Authorization": `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	for (const enrollment of data.enrollments) {
		enrollments.push(enrollment)
	}
})

if (adminUser === "true"){
	bookNow.classList.add("d-none")
	addButton.innerHTML += 
	`
			<div class="col-md-2.5" id="addCourseDiv">
				<div class="">
					<a href="./addCourse.html" class="btn btn-primary">Add Course</a>
				</div>
			</div>
		
	`
	fetch(`${baseUrl()}/api/courses/all`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res =>res.json())
		.then(data =>{
			// data.length = 0
			if(data.length<1){
				courseData =  "No Courses Available."
			} else {
				courseData = data.map(course => {
					if(course.isActive == true){
						cardFooter = 
							`
								<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block">Archive Course</a>
							`
					} else if (course.isActive == false){
						cardFooter =
							`
								<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block">Enable Course</a>
							`
					} 
					return	(
						`
								<div class="col-md-6 my-3">
									<div class="card h-100 shadow border border-secondary">
										<div class="card-body border-bottom border-secondary">
											<h5 class="card-title">${course.name}</h5>
											<p class="card-text text-left">&#8369;${course.price}</p>
											<p class="card-text text-left">${course.description}</p>
										</div>
										<div class="card-footer">
											<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>
											<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Edit Course</a>
											${cardFooter}
										</div>
									</div>
								</div>
						`	
					)	
				}).join("")
			}
			container.innerHTML = courseData
		})		
} else {
	if (adminUser === 'false' && token) bookNow.classList.add("d-none")
	fetch(`${baseUrl()}/api/courses`).then(res=>res.json()).then(data=>{
		msg = "<b>Login to book a course. If you don't have an account, you can register for free. \nClick <b class='text-primary'>'Book Now'</b>again</b>"
		if (data.length<1){
			courseData = "No Courses Available."
		}
		else {
			courseData = data.map(course=>{
				if (adminUser === "false"){
					cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>`
					msg = ""
				} else{
					cardFooter = ''
					msg = "<b>Login to book a course. If you don't have an account, you can register for free. <br/>Click <span class='text-primary'>\"Book Now\"</span> again</b>"
				}
				let isEnrolled = enrollments.some((enrollment) => {
					return enrollment.courseId === course._id
				})
				return (
					`
						<div class="col-md-6 my-3">
							<div class="card h-100 shadow border border-secondary">
								<div class="card-body">
									<h5 class="card-title">${course.name}</h5>
									<p class="card-text text-left">&#8369;${course.price}</p>
									<p class="card-text text-primary font-weight-bold">${isEnrolled ? `Enrolled` : ''}</p>
									<p class="card-text text-left">${course.description}</p>
								</div>
								<div class="card-footer">${cardFooter}</div>
							</div>
						</div>
					`
				) 
			}).join("")
		}
		msgForGuest.innerHTML = msg
		container.innerHTML = courseData
	})
}
container.classList.add("justify-content-center")
