// We use the URLSearchParams to access the specific parts of query string in the URL
// d1/pages/deleteCourse.html?courseId=5fbb4f0cb52b6f37a47342c8
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let labelResult = document.querySelector('#labelResult')

let token = localStorage.getItem("token")

//this fetch will be run automatically once you get to the page deleteCourse page
fetch(`${baseUrl()}/api/courses/deactivate/${courseId}`, {
	method: "PUT",
	headers: {
		"Authorization": `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if(data){
		alert("Course Archived.")
		labelResult.innerHTML = 'Course archived.'
	} else {
		alert("Something Went Wrong.")
	}
})