let navItems = document.querySelector("#navSession");
let userGreeting = document.querySelector("#userGreeting")


//LocalStorage => an object used to store information indefinitely locally in our device

let userToken = localStorage.getItem("token");
let isAdmin = localStorage.getItem("isAdmin") //getting items from localstorage are string 

/* local storage is a built in function in javascript
localStorage {
	token: "123421dsfsdf123fsdf"
	isAdmin: false,
	getItem: function()
	setItem: function()
}
*/

// `` = backticks/julius back ticks/template string

//conditional rendering:
//dynamically add or delete an html element based on a condition
//Here we are adding our login and register links in the navbar ONLY if the user is not logged in.
//Otherwise, only the log out will be shown
if(!userToken) {
	navItems.innerHTML +=  // += adds another children html to the current children of the id
	`
		<li class="nav-item ">
			<a href="./pages/login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>
	`
	userGreeting.innerHTML =
	`
		Hello, Guest!
	`

} else {

	if (isAdmin == 'true'){
		navItems.innerHTML +=
		`
			<li class="nav-item">
				<a href="./pages/logout.html" class="nav-link"> Log Out </a>
			</li>	
		`
		userGreeting.innerHTML +=
		`
			Hello, Admin!
		`
	} else {
		navItems.innerHTML += 
		`
			<li class="nav-item">
				<a href="./pages/profile.html" class="nav-link"> Profile </a>
			</li>
			<li class="nav-item">
				<a href="./pages/logout.html" class="nav-link"> Log Out </a>
			</li>	
		`
		userGreeting.innerHTML +=
		`
			Hello, User!
		`
	}	
}
