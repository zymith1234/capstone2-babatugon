let params = new URLSearchParams(window.location.search) 
let courseId = params.get('courseId')
let adminUser = localStorage.getItem('isAdmin') 

let token = localStorage.getItem('token')

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

let enrollments = []
fetch(`${baseUrl()}/api/users/details`,{
	method: "GET",
	headers: {
		"Authorization": `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	for (const enrollment of data.enrollments) {
		enrollments.push(enrollment)
	}
})

fetch(`${baseUrl()}/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	if (adminUser == "false") {

		let isEnrolled = enrollments.some(enrollment => enrollment.courseId === courseId)
		if (isEnrolled) {
			enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-secondary" disabled>Already Enrolled</button>`
		} 
		else {
			enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`
	
			document.querySelector("#enrollButton").addEventListener("click",()=>{
	
				fetch(`${baseUrl()}/api/users/enroll`,{
					method:'POST',
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${token}`
					},
					body: JSON.stringify({
						courseId: courseId
	
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data){
						
						fetch(`${baseUrl()}/api/users/details`, {
							headers: {
								Authorization: `Bearer ${data.accessToken}`
							}
						})
						.then(res2 => res2.json())
						.then(data2 => {
							let coursesEnrolled = JSON.stringify(data2.enrollments)
							localStorage.setItem('courses', coursesEnrolled)
							alert("You have enrolled successfully.")
							window.location.replace("./courses.html")
						})
					} else {
						alert("Enrollment Failed.")
					}
				})
			})
		}

	} else if (adminUser == "true"){

		enrollContainer.innerHTML += `<h5 class="pt-5">Enrollees:</h5>`
	
		data.enrollees.map(enrolleesInfo=>{
			return (
				fetch(`${baseUrl()}/api/users`)
				.then(res=>res.json())
				.then(data2=>{
					data2.map(usersMatchedWithEnrollees=>{
						if (usersMatchedWithEnrollees._id === enrolleesInfo.userId){
							let date 
							date = enrolleesInfo.enrolledOn.slice(0,10)
							enrollContainer.innerHTML +=
							`
								<div class="col-sm-12 my-3">
									<div clas="card">
										<div class="card-body border border-dark shadow rounded">
											<h6 class="card-title">Name: ${usersMatchedWithEnrollees.firstName} ${usersMatchedWithEnrollees.lastName}</h6>
											<p class="card-text text-center">Date enrolled: ${date}</p>
										</div>
									</div>
								</div>
							`
						}
					})	
				})
				.catch(err => {
					alert(err)
				})
			)
		}) 
		if (!data.enrollees.length) {
			enrollContainer.innerHTML += `No enrollees`
		}
	}
})
