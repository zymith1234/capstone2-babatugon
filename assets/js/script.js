let navItems = document.querySelector("#navSession");
let userToken = localStorage.getItem("token");
let isAdmin = localStorage.getItem("isAdmin");

if(!userToken) {
	navItems.innerHTML = 
	`
		<li class="nav-item">
				<a href="./../index.html" class="nav-link"> Home </a>
		</li>
		<li class="nav-item">
				<a href="./courses.html" class="nav-link"> Courses </a>
		</li>		
		<li class="nav-item ">
			<a href="./login.html" class="nav-link"> Log in </a>
		</li>
		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
	`
} else {
	if(isAdmin == "true"){
		navItems.innerHTML = 
		`
			<li class="nav-item active">
				<a href="./../index.html" class="nav-link"> Home </a>
			</li>
			<li class="nav-item active">
				<a href="./courses.html" class="nav-link"> Courses </a>
			</li>
		`
	}
	navItems.innerHTML += 
	`
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>	
	`	
}



