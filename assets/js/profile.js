let adminUser = localStorage.getItem('isAdmin') 
let token = localStorage.getItem('token')
let profile = document.querySelector('#profileInfo')
let enrolledCourses = document.querySelector('#userEnrolledCourses')
let profileContainer = document.querySelector('#profileContainer')
let editProfileButton = document.querySelector('#editProfileButton')


if (adminUser === "false"){
	let id = localStorage.getItem('id')

	fetch(`${baseUrl()}/api/users/details`,{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	})
	.then(res=>res.json())
	.then(data=>{
		profile.innerHTML = 
		`
			<div class="col-sm-12 text-center">
				<h4 class="pt-0 mt-0">${data.firstName} ${data.lastName}</h4>
				<p class="pb-0 mb-0"></p>
				<p class="pt-0 mt-0">mobile no: ${data.mobileNo}</P>
			</div>
		`

		editProfileButton.innerHTML +=
		`
			<div class="row flex-row-reverse pr-1" id="editCourseDiv">
				<div class="mx-3">
					<a href="./editProfile.html" class="btn btn-primary">Edit Profile</a>
				</div>
			</div>
		`
		data.enrollments.map(courses=>{
			fetch(`${baseUrl()}/api/courses/${courses.courseId}`)
			.then(res2=>res2.json())
			.then(data2=>{ 
				let date = courses.enrolledOn.slice(0,10)
				return ( 
					enrolledCourses.innerHTML +=
					`
						<div class="col-md-6 my-3">
							<div clas="card">
								<div class="card-body shadow border border-secondary" id="outsideOfProfileCourseCard">
									<p class="card-title font-weight-bold" id="profileCourseTitle">${data2.name}</p>
									<p class="card-text text-left" id="profileDateEnrolled">Date enrolled: ${date}</p>
								</div>
							</div>
						</div>
					`
				)
			})
		})
		
		if (!data.enrollments.length) {
			enrolledCourses.innerHTML += 
			`
				<div class="mx-auto">No enrolled courses</div>
			`
		}
	})	
} else {
	profileContainer.innerHTML = ``
}