let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let token = localStorage.getItem("token")
let editCourseForm = document.querySelector("#editCourse");
let nameInput = document.querySelector('#courseName')
let priceInput = document.querySelector('#coursePrice')
let descriptionInput = document.querySelector('#courseDescription')

fetch(`${baseUrl()}/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	nameInput.value = data.name
	descriptionInput.value = data.description
	priceInput.value = data.price
})

editCourseForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let courseName = document.querySelector("#courseName").value
	let courseDescription = document.querySelector("#courseDescription").value
	let coursePrice = document.querySelector("#coursePrice").value

	fetch(`${baseUrl()}/api/courses/${courseId}`, {
		method: "PUT",
		headers: {
			'Content-Type': 'application/json',
			"Authorization": `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: courseDescription,
			price: coursePrice
		})
	})
	.then(res => res.json())
	.then(data => {
		if (data == true){
			alert("Update Successful")
			window.location.href = 'courses.html'
		} else {
			alert("Update Failed. Course name can't be found or server error.")
		}
	})
})