let token = localStorage.getItem("token")
let editProfileForm = document.querySelector("#editProfile");
let firstNameEdit = document.querySelector("#firstNameEdit")
let lastNameEdit = document.querySelector("#lastNameEdit")
let mobileNumberEdit = document.querySelector("#mobileNumberEdit")
let userId = localStorage.getItem("id")

fetch(`${baseUrl()}/api/users/details`,{
	method: "GET",
	headers: {
		"Authorization": `Bearer ${token}`
	}
})
.then(res=>res.json())
.then(data=>{
	firstNameEdit.value = data.firstName
	lastNameEdit.value = data.lastName
	mobileNumberEdit.value = data.mobileNo
})


editProfileForm.addEventListener("submit", (e) => {
	e.preventDefault();
	if (mobileNumberEdit.value.length > 11) {
		alert('Failed to submit. \nMobile number exceeds 11 digits.')
		return
	}
	fetch(`${baseUrl()}/api/users/${userId}`, {
		method: "PUT",
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`
		},
		body: JSON.stringify({
			firstName: firstNameEdit.value,
			lastName: lastNameEdit.value,
			mobileNo: mobileNumberEdit.value
		})
	}).then(res2=>res2.json())
	.then(data2=>{
		if (data2 == true){
			alert("Updated Successfully")
			window.location.replace('./profile.html')
		} else {
			alert("Update Failed. Server error. Check your internet connection.")
		}
	})	
})

