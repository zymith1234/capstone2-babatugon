let loginForm = document.querySelector("#logInUser")

/*add a submit event to our login form, so that when the button is clicked to submit, we are able to run a function*/
loginForm.addEventListener("submit", (e)=>{
	e.preventDefault()
	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	//check if our user was able to submit an email or password, if not, we'll show an alert, if yes, we'll proceed to the login fetch request.

	if(email == "" || password == ""){
		alert("Please input your email/passowrd.")
	} else {
		fetch(`${baseUrl()}/api/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			//store our token in our localStorage
			//localStorage is a storage for data in most modern browsers.
			//localStorage, however, can only store strings.

			//check if data/response is false or null, if there is no token or false is returned, we will not save it in the localstorage

			if(data !== false && data.access !== null){
				//if we are able to get a token, we'll the use it to get our user's details.
				//store the token in the localStorage:
				//setItem(<nameOftheKey>, <value>)
				//local storage  is found in f12 then application where you can see the token
				localStorage.setItem('token', data.accessToken)

				fetch(`${baseUrl()}/api/users/details`, {
					//if get method, it is okay not to put method: GET in the code
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data => {
					localStorage.setItem('id', data._id)
					localStorage.setItem('isAdmin', data.isAdmin)
					//redirect to our courses.html page
					window.location.replace('./courses.html')
				})
			} else {
				alert('Login Failed. Something went wrong.')
			}
		})
	}
})